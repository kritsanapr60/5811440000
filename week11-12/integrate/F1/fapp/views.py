# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

from math import e, sin

# Create your views here.
def to_dict(text):
    data = {}
    for param in text.split('&'):
        a = param.split('=')
        data[a[0]] = a[1]
    return data

def index(req):
    #print(req.method)
    x, result = 0, 0
    if req.method == 'POST':
        #print('เขา post มา')
        data = to_dict(req.body)
        try:
            x = float(data['x'])
            #print('x = ', x)
            result = e**(x*sin(2*x))
            #print('result = ', result)
        except: pass
    else: 
        print('เขา get มา')

    #return HttpResponse('เว็บผมสุดยอด')
    return render(req, 'fapp/index.html', { 'x': x, 'result': result })

def integrate(req):
    #print(req.method)
    result = {
        'n': 0,
        'a': 0.0,
        'b': 0.0,
        'h': 0.0,
        'area': 0.0
    }
    if req.method == 'POST':
        data = to_dict(req.body)
        #print('x = ', data['x'])
        try:
            result['n'] = int(data['n'])
            result['a'] = float(data['a'])
            result['b'] = float(data['b'])
            result['h'] = (result['b'] - result['a'])/result['n']
            # area = ???
        except: pass
    else: 
        print('เขา get มา')

    #return HttpResponse('เว็บผมสุดยอด')
    return render(req, 'fapp/integrate.html', result)
