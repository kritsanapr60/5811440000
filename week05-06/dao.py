import numpy as np


px = []
py = []
with open('areaprice.csv', 'r') as f:
    for line in f.readlines():
        a = line.strip().split(',') # [ '400', '400000' ]
        px.append( float(a[0]) )
        py.append( float(a[1]) )

ai = np.polyfit(px, py, 2)
regress = np.poly1d(ai)
print(f'สัมประสิทธ์ degree 2 คือ {ai}')
area = input('จะขายที่กี่ตารางวา: ')
area = float(area)
print(regress)
print(f'คุณควรจะตั้งราคาอยู่ที่ {regress(area)} บาท')
