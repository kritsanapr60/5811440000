import numpy as np
x,y = [],[]
with open('areaprice.csv', 'r') as f:
    for line in f.readlines():
        a = line.strip().split(',') 
        x.append( float(a[0]) )
        y.append( float(a[1]) )
# หาค่า b
ybar = sum(y)/len(y)
xbar = sum(x)/len(x)
p = sum([xi*yi for xi,yi in zip(x,y)])
q = sum([xi*ybar for xi in x])
r = sum([xi**2 for xi in x])
s = sum([xi*xbar for xi in x])
b = (p - q)/(r - s)
a = ybar - xbar * b
def lsq(x):
    return a*x + b
area = input('จะขายที่กี่ตารางวา: ')
area = float(area)
print(f'คุณควรจะตั้งราคาอยู่ที่ {a*area + b} บาท')
